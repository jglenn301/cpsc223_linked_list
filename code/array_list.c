#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "list.h"

struct _list
{
  element *elements;  // pointer to the array containing the elements
  size_t size;        // the number of elements actually in the list
  size_t capacity;    // the max elements the current array can hold
};

struct _iterator
{
  size_t curr;
  list * list;
};

#define LIST_INITIAL_CAPACITY (2)
#define LIST_MINIMUM_CAPACITY (2)

// spare functions from earlier implementation
bool list_add_at(list *l, element to_add, size_t i);
element list_remove_at(list *l, size_t i);
size_t list_find(const list *l, element e);

list *list_create()
{
  // make space for the meta-data struct
  list *result = malloc(sizeof(list));

  // make an array for future elements
  result->elements = malloc(sizeof(element) * LIST_INITIAL_CAPACITY);

  // initialize size and capacity
  result->size = 0;
  result->capacity = result->elements != NULL ? LIST_INITIAL_CAPACITY : 0;

  return result;
}

size_t list_size(const list *l)
{
      return l->size;
}

element list_get(list *l, size_t i)
{
  return l->elements[i];
}

element list_set(list *l, element e, size_t i)
{
  element old;
  old = l->elements[i];

  l->elements[i] = e;

  return old;
}

bool list_add_end(list *l, element to_add)
{
  return list_add_at(l, to_add, l->size);
}

bool list_add_front(list *l, element to_add)
{
  return list_add_at(l, to_add, 0);
}

bool list_add_at(list *l, element to_add, size_t i)
{
  // make sure there's enough space
  if (l->capacity > 0 && l->size == l->capacity)
    {
      element *bigger = realloc(l->elements, sizeof(element) * l->capacity * 2);
      if (bigger != NULL)
	{
	  l->elements = bigger;
	  l->capacity *= 2;
	}
    }
  
  if (l->size < l->capacity)
    {
      // copy elements to higher index to make room
      for (size_t copy_to = l->size; copy_to > i; copy_to--)
	{
	  l->elements[copy_to] = l->elements[copy_to - 1];
	}
      
      l->elements[i] = to_add;
      l->size++;
      return true;
    }
  else
    {
      return false;
    }
}

element list_remove_end(list *l)
{
  return list_remove_at(l, l->size - 1);
}

element list_remove_at(list *l, size_t i)
{
  element old = l->elements[i];

  for (size_t copy_to = i; copy_to < l->size - 1; copy_to++)
    {
      l->elements[copy_to] = l->elements[copy_to + 1];
    }

  l->size--;

  if (l->size < l->capacity / 4 && l->capacity > LIST_MINIMUM_CAPACITY)
    {
      size_t new_cap = l->size > LIST_MINIMUM_CAPACITY ? l->size : LIST_MINIMUM_CAPACITY;
      element *smaller = realloc(l->elements, sizeof(element) * new_cap);
      if (smaller != NULL)
	{
	  l->elements = smaller;
	  l->capacity = new_cap;
	}
    }
  
  return old;
}

bool list_contains(const list *l, element e)
{
  return (list_find(l, e) != LIST_NOWHERE);
}

size_t list_find(const list *l, element e)
{
  // sequential search for the element
  size_t i = 0;
  while (i < l->size && l->elements[i] != e)
    {
      i++;
    }

  if (i == l->size)
    {
      return LIST_NOWHERE;
    }
  else
    {
      return i;
    }

}

void list_destroy(list *l)
{
  // free the array
  free(l->elements);

  // free the meta-data struct
  free(l);
}

void list_for_each(list *l, void (*f)(element x, void *s), void *s)
{
  for (size_t i = 0; i < l->size; i++)
    {
      f(l->elements[i], s);
    }
}

iterator *list_iterator(list *l)
{
  iterator *i = malloc(sizeof(iterator));
  if (i != NULL)
    {
      i->curr = 0;
      i->list = l;
    }
  return i;
}

bool list_iterator_has_next(const iterator *i)
{
  return i->curr < i->list->size;
}

element list_iterator_next(iterator *i)
{
  element value = i->list->elements[i->curr];
  i->curr++;
  return value;
}

void list_iterator_destroy(iterator *i)
{
  free(i);
}
