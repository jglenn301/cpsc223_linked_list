#ifndef __LIST_H__
#define __LIST_H__

#include <stdbool.h>

#define LIST_NOWHERE ((size_t)-1)

/**
 * The type of the items in the list.  We use this here so we can
 * easily change the type of the list, although with this setup we
 * can still only have one type of list per program.  Since elements
 * are copied into the various functions, it is recommended to only
 * use this with primitive and pointer types.  And for pointers, 
 * note that the list does not take ownership of the pointers added to
 * it, so freeing anything they point to remains the responsibility
 * of the user of the list.
 */
typedef int element;

typedef struct _list list;

typedef struct _iterator iterator;

/**
 * Creates an empty list.  Returns NULL if there is a memory allocation
 * failure.  It is the caller's responsibility to eventually destroy
 * the list if it is non-NULL.
 *
 * @return a pointer to a list, or NULL
 */
list *list_create();

/**
 * Returns the size of the given list.
 *
 * @param l a pointer to a list, non-NULL
 */
size_t list_size(const list *l);

/**
 * Returns the element at the given location in this list.
 *
 * @param l a pointer to a list, non-NULL
 * @param i an index into that list
 * @return an element
 */
element list_get(list *l, size_t i);

/**
 * Adds the given element to the end of the given list.  Returns true
 * if the element was added and false if not (if there was a memory allocation
 * error).
 *
 * @param l a pointer to a list, non-NULL
 * @param to_add an element
 * @return a boolean
 */
bool list_add_end(list *l, element to_add);

/**
 * Adds the given elements at the given position in the given list.
 * All existing elements at or after that index are moved one spot
 * later (to higher indices) to make room for the new element.
 *
 * @param l a pointer to a list, non-NULL
 * @param to_add an element
 * @param i an index into the list
 * @return a boolean
 */
bool list_add_front(list *l, element to_add);

/**
 * Removes the element from the given location in the given list.  All
 * elements after that index are moved one spot earlier (to lower
 * indices) to fill the space left by the removed element.
 *
 * @param l a pointer to a list, non-NULL
 * @param to_add an element
 * @param i an index into the list
 * @return an element
 */
element list_remove_end(list *l);

/**
 * Destroys the given list.
 *
 * @param l a pointer to a list, non-NULL
 */
void list_destroy(list *l);

/**
 * Calls the given function on each element in the given list
 * in order starting with the first element.  The last argument
 * is passed along with the elements to the function.
 *
 * @param l a pointer to a list, non-NULL
 * @param f a function that takes an element and a pointer and returns
 * nothing, non-NULL
 * @param s a pointer
 */
void list_for_each(list *l, void (*f)(element x, void *s), void *s);

/**
 * Returns an iterator positioned at the first element in the given list.
 * Returns NULL if there is an error allocating memory.  It is the
 * caller's responsibility to eventually destroy the returned iterator if
 * it is non-NULL.
 *
 * @param l a pointer to a list, non-NULL
 */
iterator *list_iterator(list *l);

/**
 * Determines if the given iterator is positioned within the list.
 *
 * @param i a pointer to an iterator, non-NULL
 * @return true if and only if there is a next element to retrieve through i
 */
bool list_iterator_has_next(const iterator *i);

/**
 * Returns the element at the given iterator's current position in the list
 * and advances the iterator to the next element.
 *
 * @param i a pointer to an iterator at a valid position, non-NULL
 * @return an element
 */
element list_iterator_next(iterator *i);

/**
 * Destroys the given iterator.
 *
 * @param i a pointer to an iterator, non-NULL
 */
void list_iterator_destroy(iterator *i);

#endif
