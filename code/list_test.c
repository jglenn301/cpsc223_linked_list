#include <stdlib.h>
#include <stdio.h>

#include "list.h"

// Functions to pass to list_for_each
void sum_int(int x, void *sum);
void print_int(int x, void *s);

int main(int argc, char **argv)
{
  list *l = list_create();
  int max = atoi(argv[1]);
  
  for (size_t i = 1; i < max; i++)
    {
      list_add_end(l, i);
    }

  printf("Printing using for_each\n");
  list_for_each(l, print_int, "%3d\n");

  // loop is O(n) for array list but O(n^2) for linked list
  int sum = 0;
  for (size_t i = 0; i < list_size(l); i++)
    {
      sum += list_get(l, i);
    }
  printf("sum using get = %d\n", sum);


  sum = 0;
  iterator *i = list_iterator(l);
  while (list_iterator_has_next(i))
    {
      sum += list_iterator_next(i);
    }
  printf("sum using iterator = %d\n", sum);
  list_iterator_destroy(i);

  sum = 0;
  list_for_each(l, sum_int, &sum);
  printf("sum using for each = %d\n", sum);

  
  list_destroy(l);
}

void sum_int(int x, void *ptr)
{
  int *sum = (int *)ptr;
  *sum += x;
}

void print_int(int x, void *s)
{
  printf((char *)s, x);
}
