#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "list.h"

typedef struct _node
{
  element data;
  struct _node *prev;
  struct _node *next;
} node;

struct _list
{
  node *head;
  node *tail;
  size_t size; 
};

struct _iterator
{
  node *curr;
};

list *list_create()
{
  list *result = malloc(sizeof(list));

  if (result != NULL)
    {
      result->head = malloc(sizeof(node));
      result->tail = malloc(sizeof(node));

      if (result->head == NULL || result->tail == NULL)
	{
	  free(result->head);
	  free(result->tail);
	  free(result);
	  return NULL;
	}
      
      result->head->prev = NULL;
      result->tail->next = NULL;
      result->head->next = result->tail;
      result->tail->prev = result->head;
      
      result->size = 0;
    }
      
  return result;
}

size_t list_size(const list *l)
{
  return l->size;
}

element list_get(list *l, size_t i)
{
  node *curr = l->head->next;
  for (size_t j = 0; j < i; j++)
    {
      curr = curr->next;
    }
  return curr->data;
}

bool list_add_end(list *l, element to_add)
{
  node *new_node = malloc(sizeof(node));
  if (new_node == NULL)
    {
      return false;
    }
  
  new_node->data = to_add;
  new_node->next = l->tail;
  new_node->prev = l->tail->prev;
  l->tail->prev->next = new_node;
  l->tail->prev = new_node;
  
  l->size++;
  
  return true;
}

bool list_add_front(list *l, element to_add)
{
  // not implemented here; see stack and modify for doubly-linked with
  // dummy nodes
  return false;
}

element list_remove_end(list *l)
{
  node *delete_me = l->tail->prev;
  element value = delete_me->data;
  delete_me->prev->next = delete_me->next;
  delete_me->next->prev = delete_me->prev;
  free(delete_me);
  l->size--;
  
  return value;
}

void list_destroy(list *l)
{
  node *curr = l->head;
  while (curr != NULL)
    {
      node *next = curr->next;
      free(curr);
      curr = next;
    }
  free(l);
}

void list_for_each(list *l, void (*f)(element x, void *s), void *s)
{
  node *curr = l->head->next;
  while (curr != l->tail)
    {
      f(curr->data, s);
      curr = curr->next;
    }
}

iterator *list_iterator(list *l)
{
  iterator *i = malloc(sizeof(iterator));
  if (i != NULL)
    {
      i->curr = l->head->next;
    }
  return i;
}

bool list_iterator_has_next(const iterator *i)
{
  return i->curr->next != NULL;
}

element list_iterator_next(iterator *i)
{
  element value = i->curr->data;
  i->curr = i->curr->next;
  return value;
}

void list_iterator_destroy(iterator *i)
{
  free(i);
}
